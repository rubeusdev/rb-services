<?php
namespace Rubeus\Servicos\Classe;

class Classe{
    private $attr;
    
    public function __construct($classe=false) {
        if($classe)$this->attr = new \ReflectionClass($classe);
    }
    
    public function substituir($classe){
        $this->attr = new \ReflectionClass($classe);
    }
    
    public function getNome(){
        return $this->attr->getShortName();
    }
    
    public function getClasse(){
        return $this->attr->getName();
    }
    
    public function getNamespace(){
        return $this->attr->getNamespaceName();
    }
    
    public function getDiretorio(){
        return str_replace($this->getNome().'.php','',$this->attr->getFileName());
    }
    
    public function newInstancia($parametros){
        if(is_null($parametros))return $this->attr->newInstance();
        if(!is_array($parametros))$parametros = array();
        return $this->attr->newInstanceArgs($parametros);
    }
    
    public function isAbstract(){
        return $this->attr->isAbstract();
    }
    
    public function chamarMetodo($classe, $metodo, $parametros=false){
        if(!is_array($parametros))$parametros = array();
        $metodos = get_class_methods($classe);
        if(!in_array($metodo, $metodos))return false;
        call_user_func_array(array($classe,$metodo), $parametros);
        return true;
    }
}
