<?php
namespace Rubeus\Servicos\Boleto;

class LinhaDigitavel{
    private $banco;
    private $vencimento;
    private $valor;
    private $convenio;
    private $nossoNumero;
    private $agencia;
    private $conta;
    private $carteira;
    private $codigoCliente;
    
    public function __construct() {
        $this->especializacao=[
            '356' => new LinhaDigitavelSantander(),
            '033' => new LinhaDigitavelSantander(),
            '001' => new LinhaDigitavelBancoBrasil(),
            '341' => new LinhaDigitavelItau()
        ];
    }
    
    public function getBanco() {
        return $this->banco;
    }

    public function getVencimento() {
        return $this->vencimento;
    }

    public function getValor() {
        return $this->valor;
    }

    public function getConvenio() {
        return $this->convenio;
    }

    public function getNossoNumero() {
        return $this->nossoNumero;
    }

    public function getAgencia() {
        return $this->agencia;
    }

    public function getConta() {
        return $this->conta;
    }

    public function getCarteira() {
        return $this->carteira;
    }

    public function getCodigoCliente() {
        return $this->codigoCliente;
    }

    public function setBanco($banco) {
        $this->banco = $banco;
        return $this;
    }

    public function setVencimento($vencimento) {
        $this->vencimento = $vencimento;
        return $this;
    }

    public function setValor($valor) {
        $this->valor = $valor;
        return $this;
    }

    public function setConvenio($convenio) {
        $this->convenio = $convenio;
        return $this;
    }

    public function setNossoNumero($nossoNumero) {
        $this->nossoNumero = $nossoNumero;
        return $this;
    }

    public function setAgencia($agencia) {
        $this->agencia = $agencia;
        return $this;
    }

    public function setConta($conta) {
        $this->conta = $conta;
        return $this;
    }

    public function setCarteira($carteira) {
        $this->carteira = $carteira;
        return $this;
    }

    public function setCodigoCliente($codigoCliente) {
        $this->codigoCliente = $codigoCliente;
        return $this;
    }

    public function gerar(){
        return $this->especializacao[$this->banco]->gerarCodigo($this);
    }
}
