<?php
namespace Rubeus\Servicos\Boleto;

class LinhaDigitavelBancoBrasil{
    //private $banco;
    //private $vencimento;
    private $valor;
    private $convenio;
    private $nossoNumero;
    private $agencia;
    private $conta;
    private $codigoCliente;
  
    private $fatorVencimento;
    private $codigoBanco = "001";
    private $numMoeda = "9";
    private $fixo     = "9"; 
    private $zerosLivre = "000000";   

    public function gerarCodigo($dadosLinha){
        $this->valor = $this->formata_numero(number_format(floatval($dadosLinha->getValor()), 2, ',', ''),10,0,"valor");
        
        $this->agencia = $this->formata_numero($dadosLinha->getAgencia(),4,0);
        
        $this->conta = $this->formata_numero($dadosLinha->getConta(),8,0);
        
        $this->fatorVencimento = $this->fator_vencimento($dadosLinha->getVencimento());
        
        switch (strlen($dadosLinha->getConvenio())) {
            case 8:
                $this->convenio = $this->formata_numero($dadosLinha->getConvenio(),8,0,'convenio');
                $this->nossoNumero = $this->formata_numero($dadosLinha->getNossoNumero(),9,0);
                $dv = $this->modulo_11($this->codigoBanco.$this->numMoeda,$this->fatorVencimento.$this->valor.$this->zerosLivre.$this->convenio.$this->nossoNumero.$dadosLinha->getCarteira());
                
                $linha = $this->codigoBanco.$this->numMoeda.$dv.$this->fatorVencimento.$this->valor.$this->zerosLivre.$this->convenio.$this->nossoNumero.$dadosLinha->getCarteira();
            break;
            case 7:
                $this->convenio = $this->formata_numero($dadosLinha->getConvenio(),7,0,'convenio');
                $this->nossoNumero = $this->formata_numero($dadosLinha->getNossoNumero(),10,0);
                $dv = $this->modulo_11($this->codigoBanco.$this->numMoeda,$this->fatorVencimento.$this->valor.$this->zerosLivre.$this->convenio.$this->nossoNumero.$dadosLinha->getCarteira());
                
                $linha = $this->codigoBanco.$this->numMoeda.$dv.$this->fatorVencimento.$this->valor.$this->zerosLivre.$this->convenio.$this->nossoNumero.$dadosLinha->getCarteira();
            break;
            case 6:
                $this->convenio = $this->formata_numero($dadosLinha->getConvenio(),6,0,'convenio');
                
                if (strlen($dadosLinha->getNossoNumero()) <= 5) {
                    $this->nossoNumero = $this->formata_numero($dadosLinha->getNossoNumero(),5,0);
                    $dv = $this->modulo_11($this->codigoBanco.$this->numMoeda,$this->fatorVencimento.$this->valor.$this->convenio.$this->nossoNumero.$this->agencia.$this->conta.$dadosLinha->getCarteira().'21');

                    $linha = $this->codigoBanco.$this->numMoeda.$dv.$this->fatorVencimento.$this->valor.$this->convenio.$this->nossoNumero.$this->agencia.$this->conta.$dadosLinha->getCarteira().'21';
                } 
                else {
                    $this->nossoNumero = $this->formata_numero($dadosLinha->getNossoNumero(),17,0);
                    $dv = $this->modulo_11($this->codigoBanco.$this->numMoeda.$this->fatorVencimento.$this->valor.$this->convenio.$this->nossoNumero.'21');
                    
                    $linha = $this->codigoBanco.$this->numMoeda.$dv.$this->fatorVencimento.$this->valor.$this->convenio.$this->nossoNumero.'21';
                }
            break;
        }
        
        return $this->monta_linha_digitavel($linha);
    }


    public function modulo_10($num) { 
        $numtotal10 = 0;
	$fator = 2;
 
	for ($i = strlen($num); $i > 0; $i--) {
		$numeros[$i] = substr($num,$i-1,1);
		$parcial10[$i] = $numeros[$i] * $fator;
		$numtotal10 .= $parcial10[$i];
		if ($fator == 2) {
			$fator = 1;
		}
		else {
			$fator = 2; 
		}
	}
	
	$soma = 0;
	for ($i = strlen($numtotal10); $i > 0; $i--) {
		$numeros[$i] = substr($numtotal10,$i-1,1);
		$soma += $numeros[$i]; 
	}
	$resto = $soma % 10;
	$digito = 10 - $resto;
	if ($resto == 0) {
		$digito = 0;
	}

	return $digito;

    }



    public function formata_numero($numero,$loop,$insert,$tipo = "geral") {
        if ($tipo == "geral") {
		$numero = str_replace(",","",$numero);
		while(strlen($numero)<$loop){
			$numero = $insert . $numero;
		}
	}
	if ($tipo == "valor") {
		/*
		retira as virgulas
		formata o numero
		preenche com zeros
		*/
		$numero = str_replace(",","",$numero);
		while(strlen($numero)<$loop){
			$numero = $insert . $numero;
		}
	}
	if ($tipo == "convenio") {
		while(strlen($numero)<$loop){
			$numero = $numero . $insert;
		}
	}
	return $numero;
    }


    public function fator_vencimento($data) {
        $data = explode("/",$data);
	$ano = $data[2];
	$mes = $data[1];
	$dia = $data[0];
        return(abs(($this->_dateToDays("1997","10","07")) - ($this->_dateToDays($ano, $mes, $dia))));
    }

    public function _dateToDays($year,$month,$day) {
        $century = substr($year, 0, 2);
        $year = substr($year, 2, 2);
        if ($month > 2) {
            $month -= 3;
        } else {
            $month += 9;
            if ($year) {
                $year--;
            } else {
                $year = 99;
                $century --;
            }
        }

        return ( floor((  146097 * $century)    /  4 ) +
                floor(( 1461 * $year)        /  4 ) +
                floor(( 153 * $month +  2) /  5 ) +
                    $day +  1721119);
    }


    public function modulo_11($num, $base=9, $r=0)  {
       
        $soma = 0;
	$fator = 2; 
	for ($i = strlen($num); $i > 0; $i--) {
		$numeros[$i] = substr($num,$i-1,1);
		$parcial[$i] = $numeros[$i] * $fator;
		$soma += $parcial[$i];
		if ($fator == $base) {
			$fator = 1;
		}
		$fator++;
	}
        
	if ($r == 0) {
		$soma *= 10;
		$digito = $soma % 11;
		
		//corrigido
		if ($digito == 10) {
			$digito = "X";
		}

		/*
		alterado por mim, Daniel Schultz

		Vamos explicar:

		O módulo 11 só gera os digitos verificadores do nossonumero,
		agencia, conta e digito verificador com codigo de barras (aquele que fica sozinho e triste na linha digitável)
		só que é foi um rolo...pq ele nao podia resultar em 0, e o pessoal do phpboleto se esqueceu disso...
		
		No BB, os dígitos verificadores podem ser X ou 0 (zero) para agencia, conta e nosso numero,
		mas nunca pode ser X ou 0 (zero) para a linha digitável, justamente por ser totalmente numérica.

		Quando passamos os dados para a função, fica assim:

		Agencia = sempre 4 digitos
		Conta = até 8 dígitos
		Nosso número = de 1 a 17 digitos

		A unica variável que passa 17 digitos é a da linha digitada, justamente por ter 43 caracteres

		Entao vamos definir ai embaixo o seguinte...

		se (strlen($num) == 43) { não deixar dar digito X ou 0 }
		*/
		
		if (strlen($num) == "43") {
			//então estamos checando a linha digitável
			if ($digito == "0" || $digito == "X" || $digito > 9) {
					$digito = 1;
			}
		}
                
		return $digito;
	} 
	elseif ($r == 1){
		$resto = $soma % 11;
		return $resto;
	}
    }



    public function monta_linha_digitavel($linha) 
    { 
        // Posição 	Conteúdo
        // 1 a 3    Número do banco
        // 4        Código da Moeda - 9 para Real
        // 5        Digito verificador do Código de Barras
        // 6 a 19   Valor (12 inteiros e 2 decimais)
        // 20 a 44  Campo Livre definido por cada banco
        // 1. Campo - composto pelo código do banco, código da moéda, as cinco primeiras posições
        // do campo livre e DV (modulo10) deste campo
        $p1 = substr($linha, 0, 4);
        $p2 = substr($linha, 19, 5);
        $p3 = $this->modulo_10("$p1$p2");
        $p4 = "$p1$p2$p3";
        $p5 = substr($p4, 0, 5);
        $p6 = substr($p4, 5);
        $campo1 = "$p5.$p6";
        // 2. Campo - composto pelas posiçoes 6 a 15 do campo livre
        // e livre e DV (modulo10) deste campo
        $p1 = substr($linha, 24, 10);
        $p2 = $this->modulo_10($p1);
        $p3 = "$p1$p2";
        $p4 = substr($p3, 0, 5);
        $p5 = substr($p3, 5);
        $campo2 = "$p4.$p5";
        // 3. Campo composto pelas posicoes 16 a 25 do campo livre
        // e livre e DV (modulo10) deste campo
        $p1 = substr($linha, 34, 10);
        $p2 = $this->modulo_10($p1);
        $p3 = "$p1$p2";
        $p4 = substr($p3, 0, 5);
        $p5 = substr($p3, 5);
        $campo3 = "$p4.$p5";
        // 4. Campo - digito verificador do codigo de barras
        $campo4 = substr($linha, 4, 1);
        // 5. Campo composto pelo valor nominal pelo valor nominal do documento, sem
        // indicacao de zeros a esquerda e sem edicao (sem ponto e virgula). Quando se
        // tratar de valor zerado, a representacao deve ser 000 (tres zeros).
        $campo5 = substr($linha, 5, 14);
        return "$campo1 $campo2 $campo3 $campo4 $campo5"; 
    }

}
