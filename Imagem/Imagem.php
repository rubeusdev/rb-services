<?php

namespace Rubeus\Servicos\Imagem;

use Rubeus\Servicos\Json\Json;
use Rubeus\Servicos\Entrada\Sessao;
use Rubeus\Arquivos\Arquivo;

class Imagem
{
    private $img;
    private $img_temp;
    private $largura;
    private $altura;
    private $novaLargura;
    private $novaAltura;
    private $tamanhoHtml;
    private $formato;
    private $extensao;
    private $tamanho;
    private $arquivo;
    private $endImagem;
    private $diretorio;
    private $qualidade;
    private $extensoesValidas;
    private $posicaoCrop;
    private $erro;
    private $caminhoImagem;
    private $dimensoes;
    private $origem;
    private $configImagem;
    private $blur;
    private $circle;

    public function __construct($origem = false, $diretorio = "../arquivos_temporarios/", $tipo = 1)
    {
        $this->origem = $origem;
        $this->extensoesValidas = array('jpg', 'jpeg', 'png');
        $this->posicaoCrop = null;
        $this->qualidade = 90;
        $this->erro = array();
        $this->configImagem = Json::lerArq(DIR_BASE . '/' . CONFIG_IMAGEM, false, true);
        $this->setDimenssoes($tipo);
        $this->diretorio = $diretorio;
    }

    public function getError()
    {
        return $this->erro;
    }

    public function setDimenssoes($tipo = 0, $larg = false, $alt = false)
    {
        $achou = false;
        foreach ($this->configImagem as $config) {
            if ($config['tipo'] == $tipo) {
                $achou = true;
                $this->dimensoes = $config['dimensoes'];
                if (isset($config['blur'])) {
                    $this->blur = $config['blur'];
                }
                if (isset($config['circle'])) {
                    $this->circle = $config['circle'];
                }
                break;
            }
        }
        if (!$achou) {
            $this->dimensoes = array(
                array('altura' => $larg, 'largura' => $alt, 'suf' => '-use'),
                array('altura' => '100%', 'largura' => '100%', 'suf' => '-origem')
            );
            $this->blur = false;
        }
    }

    public function setDiretorio($diretorio)
    {
        $this->diretorio = $diretorio;
    }

    private function dados2($imagem)
    {
        $this->origem = $imagem;
        if (!is_file($this->origem)) {
            $this->erro[] = 'Erro: Arquivo de imagem não encontrado!';
        } else {
            $this->dadosArquivo2();
            if (!$this->eImagem()) {
                $this->erro[] = 'Erro: Arquivo ' . $this->origem . ' não é uma imagem!';
            } else {
                $this->dimensoes();
                $this->criaImagem();
            }
        }
    }


    private function dados($imagem)
    {
        $this->origem = $imagem;
        if (!is_file($this->origem['tmp_name'])) {
            $this->erro[] = 'Erro: Arquivo de imagem não encontrado!';
        } else {
            $this->dadosArquivo();
            if (!$this->eImagem()) {
                $this->erro[] = 'Erro: Arquivo ' . $this->origem . ' não é uma imagem!';
            } else {
                $this->dimensoes();
                $this->criaImagem();
            }
        }
    }

    private function criaImagem()
    {
        switch ($this->formato) {
            case 1:
                $this->img = imagecreatefromgif($this->endImagem);
                $this->extensao = 'gif';
                break;
            case 2:
                $this->img = imagecreatefromjpeg($this->endImagem);
                $this->extensao = 'jpg';
                break;
            case 3:
                $this->img = imagecreatefrompng($this->endImagem);
                $this->extensao = 'png';
                break;
            case 6:
                $this->img = imagecreatefrombmp($this->endImagem);
                $this->extensao = 'bmp';
                break;
            default:
                $this->erro[] = 'Arquivo inválido!';
                break;
        }
    }

    private function criarImagemReal()
    {
        switch ($this->formato) {
            case 1:
                $valido = imagegif($this->img_temp, $this->caminhoImagem);
                $this->extensao = 'gif';
                break;
            case 2:
                $valido = imagejpeg($this->img_temp, $this->caminhoImagem);
                $this->extensao = 'jpg';
                break;
            case 3:
                $valido = imagepng($this->img_temp, $this->caminhoImagem);
                $this->extensao = 'png';
                break;
            default:
                $this->erro[] = 'Arquivo inválido!';
                break;
        }
        imagedestroy($this->img_temp);
    }

    private function dimensoes()
    {
        $dimensoes = getimagesize($this->endImagem);

        $this->largura = $dimensoes[0];
        $this->altura = $dimensoes[1];
        $this->formato = $dimensoes[2];
        $this->tamanhoHtml = $dimensoes[3];
    }

    private function dadosArquivo()
    {
        $tipo = explode('.', $this->origem["name"]);
        $this->extensao = $tipo[count($tipo) - 1];
        $this->arquivo    = $this->origem['name'];
        $this->endImagem = $this->origem['tmp_name'];
        $this->tamanho = $this->origem['size'];
    }

    private function dadosArquivo2()
    {
        $pathinfo = pathinfo($this->origem);
        $this->extensao = strtolower($pathinfo['extension']);
        $this->arquivo    = $pathinfo['basename'];
        $this->endImagem = $pathinfo['dirname'] . '/' . $pathinfo['basename'];
        $this->tamanho = filesize($this->origem);
    }

    private function eImagem()
    {
        return in_array($this->extensao, $this->extensoesValidas);
    }


    public function redimensiona($nova_largura = 0, $nova_altura = 0, $tipo = '')
    {
        $this->novaLargura = $nova_largura;
        $this->novaAltura = $nova_altura;


        $pos = strpos($this->novaLargura, '%');
        if ($pos !== false && $pos > 0) {
            $porcentagem = ((int) str_replace('%', '', $this->novaLargura)) / 100;
            $this->novaLargura = round($this->largura * $porcentagem);
        }
        $pos = strpos($this->novaAltura, '%');
        if ($pos !== false && $pos > 0) {
            $porcentagem = ((int) str_replace('%', '', $this->novaAltura)) / 100;
            $this->novaAltura = $this->altura * $porcentagem;
        }

        if (!($tipo == 'crop')) {
            $perAlt = $this->novaAltura / $this->altura;
            $perLar = $this->novaLargura / $this->largura;

            if ($perAlt > $perLar) {
                $this->novaAltura = round($perLar * $this->altura);
            } else {
                $this->novaLargura = round($perAlt * $this->largura);
            }
        }

        if (!$this->novaLargura && !$this->novaAltura) {
            return false;
        } elseif (!$this->novaLargura) {
            $this->novaLargura = $this->largura / ($this->altura / $this->novaAltura);
        } elseif (!$this->novaAltura) {
            $this->novaAltura = $this->altura / ($this->largura / $this->novaLargura);
        }
        switch ($tipo) {
            case 'crop':
                $this->crop();
                break;
            default:
                $this->resize();
                break;
        }
    }

    private function resize()
    {
        $imagem = new \Imagick($this->endImagem);
        $imagem->resizeImage(
            $this->novaLargura,
            $this->novaAltura,
            \Imagick::FILTER_LANCZOS,
            1.0,
            true
        );
        if ($this->blur == 1) {
            $imagem->blurImage(1, 4);
        }
        if ($this->circle == 1) {
            $draw = new \ImagickDraw();
            $draw->circle(
                $this->novaLargura / 2,
                $this->novaAltura / 2,
                $this->novaLargura / 2,
                1
            );
            $imagem->drawImage($draw);
        }
        Arquivo::criaArquivo($imagem, $this->caminhoImagem, false, 'limpo', true, 'private');
    }

    private function crop()
    {
        //echo "convert $this->caminhoImagem -blur=0x2 ".(str_replace('.','blur.',$this->caminhoImagem))."";
        exec("convert $this->endImagem -crop " . $this->posicaoCrop[2] . "x" . $this->posicaoCrop[3] . "+" . $this->posicaoCrop[0] . "+" . $this->posicaoCrop[1] . "   $this->caminhoImagem");
        if ($this->blur == 1) {
            exec("convert $this->caminhoImagem -blur '1x4' " . (str_replace('.', '-blur.', $this->caminhoImagem)) . "");
        }
    }

    public function salvarImagem($foto)
    {
        $this->dados($foto);
        $nome_imagem = md5(uniqid(time())) . "." . $this->extensao;
        $this->caminhoImagem = $this->diretorio . $nome_imagem;

        Arquivo::moveArquivo($foto["tmp_name"], $this->caminhoImagem, true, true);
        $this->img_temp = move_uploaded_file($foto["tmp_name"], $this->caminhoImagem);
        if ($this->img_temp) {
            $this->criarImagemReal();
            return $this->caminhoImagem;
        } else {
            $this->error[] = "A imagem não pode ser movida.";
        }
        return false;
    }

    public function cortarImagemFixa($diretorio)
    {
        $this->setDiretorio($diretorio);
        $nome = explode('/', $this->origem);
        $parteNome = str_replace(array('.' . $this->extensao, '.jpeg'), '', $nome[count($nome) - 1]);
        if (!defined("NAO_GERAR_COPIA_ORIGINAL") || NAO_GERAR_COPIA_ORIGINAL != 1) {
            $this->caminhoImagem = $this->diretorio . $parteNome . '-original.' . $this->extensao;
            $this->redimensiona('100%', '100%');
        }
        $this->caminhoImagem = $this->diretorio . $parteNome . '-origem.' . $this->extensao;
        $this->crop();
        $this->dados2($this->caminhoImagem);
        foreach ($this->dimensoes  as $d) {
            $this->caminhoImagem = $this->diretorio . $parteNome . $d['suf'] . '.' . $this->extensao;
            $retorno = $this->caminhoImagem;
            $this->redimensiona($d['largura'], $d['altura']);
        }
        return $retorno;
    }

    public function redimensionarImagens($diretorio)
    {
        $nome = explode('/', $this->origem);
        $parteNome = str_replace(array('.' . $this->extensao, '.jpeg'), '', $nome[count($nome) - 1]);
        $this->setDiretorio($diretorio);
        if (!defined("NAO_GERAR_COPIA_ORIGINAL") || NAO_GERAR_COPIA_ORIGINAL != 1) {
            $this->caminhoImagem = $this->diretorio . $parteNome . '-original.' . $this->extensao;
            $this->redimensiona('100%', '100%');
        }
        foreach ($this->dimensoes  as $d) {
            $this->caminhoImagem = $this->diretorio . $parteNome . $d['suf'] . '.' . $this->extensao;
            $retorno = $this->caminhoImagem;
            $this->redimensiona($d['largura'], $d['altura']);
        }
        if (defined("MENOR_DIMENSAO_MAXIMA_SALVAR_IMAGEM")) {
            if ($this->largura >= MENOR_DIMENSAO_MAXIMA_SALVAR_IMAGEM && $this->largura < $this->altura) {
                exec('convert ' . $this->endImagem . ' -resize 1080x ' . $this->endImagem);
            } elseif ($this->altura > MENOR_DIMENSAO_MAXIMA_SALVAR_IMAGEM) {
                exec('convert ' . $this->endImagem . ' -resize x1080 ' . $this->endImagem);
            }
        }
        return $retorno;
    }


    public function getImagensSessao($dados, $diretorio, $id = false, $tipo = false, $alt = false, $larg = false)
    {
        if (is_null($alt) || $alt === false) {
            $alt = '100%';
        }
        if (is_null($larg) || $larg === false) {
            $larg = '100%';
        }

        if (!($id === false)) {
            $retornoCortarImagem = $this->percorrerId($dados, $diretorio, $id);
        } else {
            $retornoCortarImagem = $this->percorrer($dados, $diretorio);
        }

        return array('success' => true, 'imagem' => $retornoCortarImagem);
    }

    private function percorrerId($dados, $diretorio, $id)
    {
        $imagem = Sessao::get('ultimasImagens,' . $id);

        $this->dados2($imagem);
        if (count($this->erro) > 0) {
            return false;
        }

        if (isset($dados['w-' . $id]) && isset($dados['h-' . $id]) && isset($dados['x-' . $id]) && isset($dados['y-' . $id])) {
            $fatorVertical = $this->altura / $dados['hr-' . $id];
            $fatorHorizontal = $this->largura / $dados['wr-' . $id];

            if (is_null($fatorHorizontal) || $fatorHorizontal == 0 || $fatorVertical == 0 || is_null($fatorVertical)) {
                $fatorHorizontal = $fatorVertical = 1;
            }

            $this->posicaoCrop = array(
                $fatorHorizontal * $dados['x-' . $id],
                $fatorVertical * $dados['y-' . $id],
                $fatorHorizontal * $dados['w-' . $id],
                $fatorVertical * $dados['h-' . $id]
            );


            $retornoCortarImagem = $this->cortarImagemFixa($diretorio);
            if (is_array($retornoCortarImagem)) {
                return array('success' => false, 'error' => $retornoCortarImagem);
            }
        } else {
            $retornoCortarImagem = $this->redimensionarImagens($diretorio);
        }

        return array($retornoCortarImagem);
    }

    private function percorrer($dados, $diretorio)
    {
        $conta = 0;
        foreach (Sessao::get("ultimasImagens") as $imagem) {
            $contaImagem = Sessao::get('ultimasImagensId,' . $conta);

            $this->dados2($imagem);
            if (count($this->erro) > 0) {
                return false;
            }

            if (isset($dados['w-' . $contaImagem]) && isset($dados['h-' . $contaImagem]) && isset($dados['x-' . $contaImagem]) && isset($dados['y-' . $contaImagem])) {
                /*$fatorVertical = $this->altura/$dados['hr-'.$contaImagem];
                $fatorHorizontal = $this->largura/$dados['wr-'.$contaImagem];

                if(is_null($fatorHorizontal) || $fatorHorizontal == 0 || $fatorVertical == 0 || is_null($fatorVertical))*/
                $fatorHorizontal = $fatorVertical = 1;

                $this->posicaoCrop = array(
                    $fatorHorizontal * $dados['x-' . $contaImagem],
                    $fatorVertical * $dados['y-' . $contaImagem],
                    $fatorHorizontal * $dados['w-' . $contaImagem],
                    $fatorVertical * $dados['h-' . $contaImagem]
                );
                //var_dump($this->posicaoCrop, $fatorHorizontal, $fatorVertical,
                //    '---------------------------------------------------');

                $retornoCortarImagem[$conta] = $this->cortarImagemFixa($diretorio);
                if (is_array($retornoCortarImagem[$conta])) {
                    return array('success' => false, 'error' => $retornoCortarImagem);
                }
            } else {
                $retornoCortarImagem[$conta] = $this->redimensionarImagens($diretorio);
            }
            $conta++;
        }
        return $retornoCortarImagem;
    }
}
