<?php
namespace Rubeus\Servicos\Entrada;
use Rubeus\ContenerDependencia\Conteiner;
/*use principal\ConfProj as ConfProj;
use principal\Saida as Saida;
use controle\control\R as R;
use modelo\persistencia\Persistencia as Persistencia;*/

abstract class Sessao{
    static $estrutura;
    static $objPermissao;
    //static $limparSessao;
    static $session;
    static $controle = false;
    static $codSessao =false;

    public static function setSessionPhp($controle){

        self::$controle = $controle;
    }


    public static function limparSessao(){
      self::$session=false;
      self::$codSessao = false;
    }

    public static function getSessao(){

        if(self::$controle == 1){
            return self::$session;
        }else{
            return $_SESSION;
        }
    }

    public static function getSessaoBanco(){
        return self::$estrutura->getSessaoBanco();
    }

    public static function getEstrutura(){
        return self::$estrutura;
    }

    public static function setEstrutura($estrutura){
        self::$estrutura = $estrutura;
    }

    public static function iniciar($codSessao=false){
        if($codSessao !== false && !is_null($codSessao)){
            self::$codSessao = $codSessao;
        }
        self::$estrutura = Conteiner::get('EstruturaRegistrarSessao', false);
        if(!self::$estrutura){
            if(!self::$controle && (!isset($_GET['naoIniciarSessao']) || $_GET['naoIniciarSessao'] != 1)){
                session_start();
                if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
                    session_write_close();
                }
            }
        }else{
            if(!self::$controle){
                if(!isset($_SESSION) && (!isset($_GET['naoIniciarSessao']) || $_GET['naoIniciarSessao'] != 1)){
                    session_start();
                    if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
                        session_write_close();
                    }
                }
            }
            self::restaurarSessao(self::$codSessao);
        }
    }

    public static function restaurarSessao($codSessao){
        $retorno = self::$estrutura->restaurarSessao($codSessao);
        if($retorno){
            self::set("codSess", $retorno['codSess']);
            self::set("idSess", $retorno['idSess']);
            if(isset($retorno['dadosSessao'])){
                foreach($retorno['dadosSessao'] as $key => $value){
                    Sessao::set($key, $value);
                }
            }
        }
    }

    public  static function set($string, $valor, $continuar=false){
        if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
            session_start();
        }
        $var = explode(',',(string)$string);

        if(!self::$controle){
            if(is_array($var) && !empty($var)){
                if(count($var)==1){
                    if($continuar)
                        $_SESSION[$var[0]][] = $valor;
                    else $_SESSION[$var[0]] = $valor;
                }else if(count($var)==2){
                    if($continuar)
                        $_SESSION[$var[0]][$var[1]][] = $valor;
                    else $_SESSION[$var[0]][$var[1]] = $valor;
                }else if(count($var)==3){
                    if($continuar)
                        $_SESSION[$var[0]][$var[1]][$var[2]][] = $valor;
                    else $_SESSION[$var[0]][$var[1]][$var[2]] = $valor;
                }else if(count($var)==4){
                    if($continuar)
                        $_SESSION[$var[0]][$var[1]][$var[2]][$var[3]][] = $valor;
                    else $_SESSION[$var[0]][$var[1]][$var[2]][$var[3]] = $valor;
                }else if(count($var)==5){
                    if($continuar)
                        $_SESSION[$var[0]][$var[1]][$var[2]][$var[3]][$var[4]][] = $valor;
                    else $_SESSION[$var[0]][$var[1]][$var[2]][$var[3]][$var[4]] = $valor;
                }
            }else $_SESSION[$var] = $valor;
        }else{
            if(is_array($var) && !empty($var)){
                if(count($var)==1){
                    if($continuar)
                        self::$session[$var[0]][] = $valor;
                    else self::$session[$var[0]] = $valor;
                }else if(count($var)==2){
                    if($continuar)
                        self::$session[$var[0]][$var[1]][] = $valor;
                    else self::$session[$var[0]][$var[1]] = $valor;
                }else if(count($var)==3){
                    if($continuar)
                        self::$session[$var[0]][$var[1]][$var[2]][] = $valor;
                    else self::$session[$var[0]][$var[1]][$var[2]] = $valor;
                }else if(count($var)==4){
                    if($continuar)
                        self::$session[$var[0]][$var[1]][$var[2]][$var[3]][] = $valor;
                    else self::$session[$var[0]][$var[1]][$var[2]][$var[3]] = $valor;
                }else if(count($var)==5){
                    if($continuar)
                        self::$session[$var[0]][$var[1]][$var[2]][$var[3]][$var[4]][] = $valor;
                    else self::$session[$var[0]][$var[1]][$var[2]][$var[3]][$var[4]] = $valor;
                }
            }else self::$session[$var] = $valor;
        }
        if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
            session_write_close();
        }
    }

    public static function limpar($string){
//        $var = explode(',',$string);
//        if(is_array($var)){
//            if(count($var)==1)unset($_SESSION[$var[0]]);
//            else if(count($var)==2)unset($_SESSION[$var[0]][$var[1]]);
//            else if(count($var)==3)unset($_SESSION[$var[0]][$var[1]][$var[2]]);
//            else if(count($var)==4)unset($_SESSION[$var[0]][$var[1]][$var[2]][$var[3]]);
//            else if(count($var)==5)unset($_SESSION[$var[0]][$var[1]][$var[2]][$var[3]][$var[4]]);
//        }else unset($_SESSION[$var]);
    }

    public static function get($string, $valorPadrao=false){
        $var = explode(',',$string);
        if(isset($_SESSION) && self::$controle != 1){
            $aux = $_SESSION;
        }else{
            $aux = self::$session;
        }
        foreach($var as $v){
            if(isset($aux[$v])){
                $aux = $aux[$v];
            }else return $valorPadrao;
        }
        if((!is_null($aux) && $aux && !empty($aux)) || ( $aux === 0 || $aux === '0') )return $aux ;
        return $valorPadrao;
    }

    public static function destroiSessao() {
        $_SESSION = array();
        session_destroy();
    }

    public static function permissaoConta($nivel){
        $permissaoAtual = self::get(CAMPO_PERMISSAO);
        if(strpos($nivel,$permissaoAtual) !== false)return true;
        return false;
    }
/*
    public static function limparSessao($destruir=false){
        if(is_null(self::$limparSessao))
            self::$limparSessao = Json::lerArq(ConfProj::getConfigLimparSessao());

        $array = array();
        foreach(self::$limparSessao->salvar as $l)
            if(self::get($l)) $array[$l]=self::get($l);

        $_SESSION = array();

        if(!$destruir){
            foreach(self::$limparSessao->destruir as $l)
                if(isset($array[$l])) self::set($l, $array[$l]);
        }

        foreach(self::$limparSessao->voltar as $l)
            if(isset($array[$l])) self::set($l, $array[$l]);
    }  */

}
