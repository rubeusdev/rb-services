<?php
namespace Rubeus\Servicos\Entrada;

abstract class I
{
    private static $dataJson;

    public static function conf()
    {
        if (isset($_SERVER["CONTENT_TYPE"]) && strpos($_SERVER["CONTENT_TYPE"], 'json')) {
            self::$dataJson = json_decode(file_get_contents("php://input"), true);
        } else {
            self::$dataJson = false;
        }
    }

    public static function get($nome,$valorPadrao=false){
        $retorno= filter_input(INPUT_GET, $nome);
        if((is_null($retorno) || $retorno == 'undefined' || $retorno === false || 
                $retorno == '' || empty($retorno) || $retorno == 'undefinedundefined') &&
                $retorno !== 0 && $retorno !== '0'){
            $retorno = $valorPadrao;
            if(isset($_GET[$nome]) && is_array($_GET[$nome])){
                return $_GET[$nome];
            }
        }return $retorno;
    }
    
    public static function files($nome, $valorPadrao = false)
    {
        /*return $_FILES[$nome];       
        if(strpos($nome,',')){
            $var = explode(',',$nome);
            $retorno = $_FILES;
            foreach($var as $v){
                if(isset($retorno[$v])){
                    $retorno = $retorno[$v];
                }else return $valorPadrao;
            }
        }else*/
        if (isset($_FILES[$nome])) {
            return $_FILES[$nome];
        }
        return $valorPadrao;
    }
    
    public static function post($nome, $valorPadrao = false)
    {
        if (!self::$dataJson) {
            $retorno = filter_input(INPUT_POST, $nome);
        } else if(isset(self::$dataJson[$nome])) {
            $retorno = self::$dataJson[$nome];
        } else {
            $retorno = false;
        }
        if((is_null($retorno) || $retorno == 'undefined' || $retorno === false || 
                $retorno == '' || empty($retorno) || $retorno == 'undefinedundefined') &&
                $retorno !== 0 && $retorno !== '0'){
            if(isset($_POST[$nome]) && is_array($_POST[$nome]))
                return $_POST[$nome];
            $retorno = $valorPadrao;
        }
        return $retorno;
    }
        
    public static function server($nome,$valorPadrao=false){
        $retorno = filter_input(INPUT_SERVER, $nome);
        if(is_null($retorno) || $retorno == 'undefined' || $retorno === false || 
                $retorno == '' || empty($retorno)){
            if(isset($_SERVER[$nome]) && is_array($_SERVER[$nome]))
                return $_SERVER[$nome];
            $retorno = $valorPadrao;
        }
        return $retorno;
    }
        
    public static function getDataJson()
    {
        return self::$dataJson;
    }
}
