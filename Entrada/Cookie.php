<?php
namespace Rubeus\Servicos\Entrada;

abstract class Cookie{
    
    public static function cookie($nome,$valorPadrao=false){
        $retorno = filter_input(INPUT_COOKIE, $nome);
        if(is_null($retorno) || $retorno == 'undefined' || $retorno === false || 
                $retorno == '' || empty($retorno)){
            if(is_array($_COOKIE[$nome]))
                return $_COOKIE[$nome];
            $retorno = $valorPadrao;
        }
        return $retorno; 
    }
    
    public static function setCookie($nome,$valor, $tempo=false){
        if(!$tempo)$tempo = (365 * 24 * 3600);
        setcookie($nome,$valor, (time()+ $tempo));
    }
    
    
    public static function unsetCookie($nome){
        unset($_COOKIE[$nome]);
    }
    
}
