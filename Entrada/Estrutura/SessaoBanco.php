<?php
        
namespace Rubeus\Servicos\Entrada\Estrutura;
use Rubeus\ORM\Persistente as Persistente;

    class SessaoBanco extends Persistente{
        private $id = false;
        private $momento = false;
        private $ativo = false;
        private $codigo = false;
        private $dadosSessao = false; 
                
        public function getId() {
            return $this->id;
        }

        public function setId($id) {
            $this->id = $id;
        } 
                
        public function getMomento() {
            return $this->momento;
        }

        public function setMomento($momento) {
            $this->momento = $momento;
        } 
                
        public function getAtivo() {
            return $this->ativo;
        }

        public function setAtivo($ativo) {
            $this->ativo = $ativo;
        } 
                
        public function getCodigo() {
            return $this->codigo;
        }

        public function setCodigo($codigo) {
            $this->codigo = $codigo;
        } 
                
        public function getDadosSessao() {
            return $this->dadosSessao;
        }

        public function setDadosSessao($dadosSessao) {
            $this->dadosSessao = $dadosSessao;
        }
        
    }