<?php
namespace Rubeus\Servicos\TipoDado\Mascara;

class CNPJ extends TipoMasc{ 
       
    public function __construct($valor=null) {
        $this->iniciar('xx.xxx.xxx/xxxx-xx',$valor);
    }
    
    public function validar(){
	$cnpj_original = $this->valor;
	$primeiros_numeros_cnpj = substr( $this->valor, 0, 12 );
	
	$primeiro_digito = $this->multiplica( $primeiros_numeros_cnpj );
	
	$primeiros_numeros_cnpj .= $primeiro_digito;
 
	$segundo_digito = $this->multiplica( $primeiros_numeros_cnpj, 6 );
	
	$this->valor = $primeiros_numeros_cnpj . $segundo_digito;
	if ( $this->valor != $cnpj_original ) 
		$this->valor = false;
    }

    private function multiplica($cnpj, $posicao = 5) {
        $calculo = 0;
        for ( $i = 0; $i < strlen( $cnpj ); $i++ ) {
            $calculo += ( $cnpj[$i] * $posicao );
            $posicao = ($posicao == 2) ? 9:$posicao-1;
        }
        return ( $calculo % 11 ) < 2 ? 0 :  11 - ( $calculo % 11 );
    }
	   
}