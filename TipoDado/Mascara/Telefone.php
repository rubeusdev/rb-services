<?php
namespace Rubeus\Servicos\TipoDado\Mascara;

class Telefone extends TipoMasc{
    private $arMasc;

    public function __construct($valor=null) {
        $this->arMasc = array('(xx)xxxxxxxx','(xx)xxxxxxxxx','(xx) xxxxxxxx','(xx) xxxxxxxxx', 'xx(xx)xxxxxxxxx', 'xx(xx)xxxxxxxx');
        if(!is_null($valor)){
            $this->identificarMascara();
            $this->iniciar($this->masc,$valor);
        }
    }

    public function set($valor) {
        $this->valor = $valor;
        $this->identificarMascara();
        $this->valor = str_replace(['(',')',' ', '-', '+'], '', $this->valor);
        return parent::set($this->valor);
    }

    private function identificarMascara(){
        foreach($this->arMasc as $k=>$v){
            if(strlen(str_replace(['(',')',' ', '-', '+'], '', $v)) == strlen(str_replace(['(',')',' ', '-', '+'],'',$this->valor))){
                $this->masc = $this->arMasc[$k];
            }
        }
    }

    public function setValidar($valor){
        $this->valor = $valor;
        $this->identificarMascara();
        $this->valor = str_replace(['(',')',' ', '-', '+'], '', $this->valor);
        return parent::setValidar($this->valor);
    }

    public function validar() {
        if (!preg_match("/^[0-9]{8,14}$/",$this->valor))
            $this->valor = false;
        //if (!preg_match("/^(?:(?:\+|00)?(55)\s?)?(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$/",$this->valor))
                 
    }

}
