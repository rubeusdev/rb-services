<?php
namespace Rubeus\Servicos\TipoDado\Mascara;

class CEP extends TipoMasc{
    private $arMasc;

    public function __construct($valor=null) {
        $this->arMasc = array('xx.xxx-xxx','xxxxx-xxx');
        if(!is_null($valor)){
            $this->identificarMascara();
            $this->iniciar($this->masc,$valor);
        }
    }

    public function validar(){
        if(!preg_match('/^[0-9]{5,5}([- ]?[0-9]{3,3})?$/', $this->valor))
            $this->valor = false;
    }

    public function set($valor) {
        $this->valor = $valor;
        $this->identificarMascara();
        return parent::set($valor);
    }

    private function identificarMascara(){
        foreach($this->arMasc as $k=>$v){
            if(strlen(str_replace(['-',' '],'',$v)) == strlen(str_replace(['-',' '],'',$this->valor))){
                $this->masc = $this->arMasc[$k];
            }
        }
    }

    public function setValidar($valor){
        $this->valor = $valor;
        $this->identificarMascara();
        return parent::setValidar($valor);
    }

}