<?php
namespace Rubeus\Servicos\TipoDado\Tipo;

class Url implements Tipo{
    private $valor;
    private $dominio;
    
    public function __construct($valor=null,$dominio='') {
        $this->set($valor,$dominio);
    }
    
    public function set($valor,$dominio=''){
        $this->valor  = $valor;
        $this->dominio = $dominio;      
        return $this;
    }
    
    public function setValidar($valor,$dominio=''){
        $this->valor  = $valor;
        $this->dominio = $dominio;
        $this->validar();        
        return $this->valor;
    }
    
    public function get(){
        return $this->valor ;
    }
    
    public function validar(){
        if(strpos($this->valor, $this->dominio) === false || $this->dominio == '')
            $this->valor = filter_var($this->valor, FILTER_VALIDATE_URL);
        else $this->valor = false;
    }
    
    public function textoUrl($texto,$antes=true){
        if($antes) return str_replace('+','%20',urlencode($texto));
        else urlencode($texto);
    }
    
}
