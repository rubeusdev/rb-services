<?php
namespace Rubeus\Servicos\TipoDado\Tipo;

class Email implements Tipo{
    private $valor;
    
    public function __construct($valor=null) {
        $this->set($valor);
    }
    
    public function set($valor){
        $this->valor  = $valor;
    }
    
    public function setValidar($valor){
        $this->valor  = $valor;
        $this->validar();        
        return $this->valor;
    }
    
    public function get(){
        return $this->valor ;
    }
    
    public function validar(){
        if (!preg_match("/^(([0-9a-zA-Z]+[-._+&])*[-.+&0-9a-zA-Z_]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,10}){0,1}$/",$this->valor))
            $this->valor = false;
        /*else{
            $dominio=explode('@',$this->valor);
            if(!checkdnsrr($dominio[1],'A'))
                $this->valor = false;
        }*/
    }
}
