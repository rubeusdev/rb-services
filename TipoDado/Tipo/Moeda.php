<?php
namespace Rubeus\Servicos\TipoDado\Tipo;

class Moeda{ 
    private $valor;
    
    public function __construct($valor=false) {
        $this->set($valor);
    }
    
    public function set($valor){
        $this->valor=$valor;
        return $this;
    }

    public function float(){
        if(!is_float($this->valor) && $this->valor){
            $caracteres = array("$", "R","r", "%");
            $this->valor = str_replace($caracteres, "", $this->valor);
            $ultimaVirgula = strripos($this->valor, ',');
            $ultimaPonto = strripos($this->valor, '.'); 
            $tamanho = strlen($this->valor)-3;
            $tamanho2 = strlen($this->valor)-2;
            $this->valor = str_replace(".", "", $this->valor);
            $this->valor = str_replace(",", "", $this->valor);
            if($ultimaPonto === $tamanho || $ultimaVirgula === $tamanho){
                $tamanho = strlen($this->valor)-2;
                $novo = $this->valor[$tamanho];
                $this->valor[$tamanho] = '.';
                $novo2 = $this->valor[$tamanho+1];
                $this->valor[$tamanho+1] = $novo;
                $this->valor .= $novo2;
            } else if($ultimaPonto === $tamanho2 || $ultimaVirgula === $tamanho2){
                $tamanho = strlen($this->valor)-1;
                $novo = $this->valor[$tamanho];
                $this->valor[$tamanho] = '.';
                $this->valor[$tamanho+1] = $novo;
            }  
            return round($this->valor,2);
        }else if(is_float($this->valor)){
            return $this->valor;
        }else return 0;
    }
	
    public function formatFloatMoeda(){
        return 'R$'.number_format($this->valor,2,",",".");
    }
}

