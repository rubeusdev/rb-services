<?php
namespace Rubeus\Servicos\TipoDado\Tipo;

interface Tipo{
    
    public function set($valor);
    
    public function get();
    
    public function validar();
}