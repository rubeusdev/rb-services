<?php
namespace Rubeus\Servicos\Localizacao;

class Distancia {
    
    function distancia($lat1, $lon1, $lat2, $lon2) {
        
        $distance = 6371 * 
                acos(cos(deg2rad($lat1)) * 
                    cos(deg2rad($lat2)) *
                    cos(deg2rad($lon1) - deg2rad($lon2)) + 
                    sin(deg2rad($lat1)) *
                    sin(deg2rad($lat2)));
        
        return $distance;
    }
}
