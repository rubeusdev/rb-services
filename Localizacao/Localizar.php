<?php
namespace Rubeus\Servicos\Localizacao;

class Localizar {
    
    public function localizar($long1, $long2, $lat1, $lat2){
        
        $d2r = 0.017453292519943295769236;
        
        $dlong = ($long2 - $long1) * $d2r;
        $dlat = ($lat2 - $lat1) * $d2r;
        
        $temp_sin = sin($dlat/2.0);
        $temp_cos = cos($lat1 * $d2r);
        $temp_sin2 = sin($dlong/2.0);
        
        $a = ($temp_sin * $temp_sin) + ($temp_cos * $temp_cos) * ($temp_sin2 * $temp_sin2);
        $c = 2.0 * atan2(sqrt($a), sqrt(1.0 - $a));
        
        return 6368.1 * $c;
    }
}
