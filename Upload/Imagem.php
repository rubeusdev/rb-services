<?php

namespace Rubeus\Servicos\Upload;

use Rubeus\Processo\Dominio\Mensagem\R as R;
use Rubeus\Servicos\Entrada\Sessao as Sessao;
use Rubeus\Servicos\Imagem\Imagem as TrabalhaImagem;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Arquivos\Arquivo;

class Imagem
{
    private $erro;

    public function getErro()
    {
        return $this->erro;
    }

    public function getMsg()
    {
        return $this->msg;
    }

    public function imagemTemporaria($foto, $id, $atributo)
    {
        if (!empty($foto["name"])) {
            $configuracao = Conteiner::get('CONFIG_UPLOAD');
            $config = $configuracao->{$atributo};
            if (defined('DIR_FILE_INSTITUICAO')) {
                $diretorioConf = str_replace('file/', 'file/' . DIR_FILE_INSTITUICAO . '/', $config->diretorio);
            } else {
                $diretorioConf = $config->diretorio;
            }
            $imagem = new TrabalhaImagem($config->origem, $diretorioConf);
            $retorno[$id] = $imagem->salvarImagem($foto);
            if (is_array($retorno[$id])) {
                $this->erro = array('erro' => $retorno[$id]);
                return false;
            }

            if (!(null !== Sessao::get('ultimasImagensId')) || !is_array(Sessao::get('ultimasImagensId'))) {
                Sessao::set('ultimasImagensId', array());
            }

            Sessao::set('ultimasImagens,' . $id, $retorno[$id]);



            $this->idUnico($id);

            return Arquivo::geraCaminho(Sessao::get('ultimasImagens,' . $id), 'url');
        }
        $this->msg = 'er_sem_img';
        return false;
    }

    private function idUnico($id)
    {
        $qtd = count(Sessao::get('ultimasImagensId'));
        $array = Sessao::get('ultimasImagensId');
        for ($i = 0; $i < $qtd; $i++) {
            if ($array[$i] == $id) {
                unset($array[$i]);
            }
        }
        $array[] = $id;
        sort($array);
        Sessao::set('ultimasImagensId', $array);
    }

    public function removerImagemSessao($id)
    {
        $qtd = count(Sessao::get('ultimasImagensId'));
        $array = Sessao::get('ultimasImagensId');
        $imagens = Sessao::get("ultimasImagens");
        for ($i = 0; $i < $qtd; $i++) {
            if ($array[$i] == $id) {
                unset($imagens[$array[$i]]);
                unset($array[$i]);
            }
        }
        sort($array);
        Sessao::set('ultimasImagensId', $array);
        Sessao::set('ultimasImagens', $imagens);
        return R::estRet(true, 'er_sem_img');
    }

    public function ImagemUpada($atributo, $pasta, $dados, $id = false, $tipo = false)
    {

        $config = Conteiner::get('CONFIG_UPLOAD')->$atributo;

        if (defined('DIR_FILE_INSTITUICAO')) {
            $diretorioConf = str_replace('file/', 'file/' . DIR_FILE_INSTITUICAO . '/', $config->diretorio);
        } else {
            $diretorioConf = $config->diretorio;
        }

        $diretorio = $diretorioConf . $pasta;
        $imagem = new TrabalhaImagem($config->origem, $diretorioConf, $tipo);
        if (!file_exists($diretorio))
            mkdir($diretorio);
        $diretorio .= "/";
        $retorno = $imagem->getImagensSessao($dados, $diretorio, $id, $tipo);

        Sessao::limpar("ultimasImagens");
        Sessao::limpar("ultimasImagensId");

        if ($retorno['success'])
            return $this->validarQtdPosicao($config, $retorno);
        return false;
    }

    private function validarQtdPosicao($config, $retorno)
    {
        if (defined('DIR_FILE_INSTITUICAO')) {
            $diretorioConf = str_replace('file/', 'file/' . DIR_FILE_INSTITUICAO . '/', $config->diretorio);
            $urlConf = str_replace('file/', 'file/' . DIR_FILE_INSTITUICAO . '/', $config->url);
        } else {
            $diretorioConf = $config->diretorio;
            $urlConf = $config->url;
        }
        if (count($retorno['imagem']) === 1) {
            $file = $urlConf . str_replace($diretorioConf, '', $retorno['imagem'][0]);
            return Arquivo::geraCaminho($file, 'url');
        } else {
            for ($i = 0; $i < count($retorno['imagem']); $i++) {
                $file = $urlConf . str_replace($diretorioConf, '', $retorno['imagem'][$i]);
                $posicoes[$i] = Arquivo::geraCaminho($file, 'url');
            }
            return $posicoes;
        }
    }
}
