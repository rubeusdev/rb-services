<?php

namespace Rubeus\Servicos\Upload;

use Rubeus\Servicos\Emcrypt\Emcrypt;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Arquivos\Arquivo;

class Upload
{
    private $erro;

    public function getErro()
    {
        return $this->erro;
    }

    public function upar($dados, $atributo, $pasta, $privado = true)
    {
        $this->erro = false;
        if (!empty($dados)) {
            $config = Conteiner::get('CONFIG_UPLOAD');
            if (!is_array($dados['tmp_name'])) {
                $dados['tmp_name'] = array($dados['tmp_name']);
                $dados['name'] = array($dados['name']);
            }
            $arquivos = array();
            $qtd = count($dados['tmp_name']);
            for ($i = 0; $i < $qtd; $i++) {
                $tipo = explode('.', $dados['name'][$i]);
                if (defined('DIR_FILE_INSTITUICAO')) {
                    $diretorioConf = str_replace('file/', 'file/' . DIR_FILE_INSTITUICAO . '/', $config->$atributo->diretorio);
                    $urlConf = str_replace('file/', 'file/' . DIR_FILE_INSTITUICAO . '/', $config->$atributo->url);
                } else {
                    $diretorioConf = $config->$atributo->diretorio;
                    $urlConf = $config->$atributo->url;
                }

                $imagem =  Emcrypt::codigo(3) . '-' . date('Y-m-d-H-i-s') . '.' . $tipo[count($tipo) - 1];
                $endereco = $diretorioConf . '/' . $pasta . '/' . $imagem;
                $url = $urlConf . '/' . $pasta . '/' . $imagem;
                mkdir($diretorioConf . '/' . $pasta, 0777, true);
                if (!Arquivo::moveArquivo($dados['tmp_name'][$i], $endereco, true, $privado)) {
                    $this->erro = ['codErro' => 'fal_arq', 'arquivo' => $dados['tmp_name'][$i]];
                    return false;
                } else {
                    $arquivos[] = [
                        'endereco' => Arquivo::geraCaminho($endereco, 'crmCaminho'),
                        'url' => Arquivo::geraCaminho($url, 'url'),
                        'nome' => $dados['name'][$i]
                    ];
                }
            }
            return $arquivos;
        }
        $this->erro = ['codErro' => 'er_sem_arq'];
        return false;
    }

    private function achaTipoMime($caminhoArquivo)
    {
        $arrayCaminhoArquivo = explode('.', $caminhoArquivo);
        $tipoMime = end($arrayCaminhoArquivo);
        switch ($tipoMime) {
            case 'csv':
                $tipoMime = 'text/csv';
                break;
            case 'jpg':
                $tipoMime = 'image/jpeg';
                break;
            default:
                $tipoMime = "image/$tipoMime";
                break;
        }
        return $tipoMime;
    }
}
