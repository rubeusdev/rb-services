<?php
namespace Rubeus\Servicos\Json;

abstract class Json{
    public static function ler($texto){
        return json_decode($texto);
    }
    
    public static function lerArq($endereco,$url=false,$assoc=false){
        if(!file_exists($endereco) && !$url) return false;
        return json_decode(file_get_contents($endereco),$assoc);
    }
    
    public static function salvarArq($endereco,$obj){
        return file_put_contents($endereco, json_encode($obj));
    }
    
    public static  function lerJsonComoArray($texto){
        return self::lerObj(json_decode($texto));
    }
    
    private static function lerObj($obj){
        $array = array();
        if(!is_object($obj) && !is_array($obj))
            return $obj;
        
        foreach($obj as $chave => $valor){
            if(is_object($valor)){
                $array[$chave] = self::lerObj($valor);
            }else if(is_array($valor)){
                for($i=0;$i< count($valor); $i++){
                    $array[$chave][$i] = self::lerObj($valor[$i]);
                }
            }else $array[$chave] = $valor;
        }
        return $array;
    }
}