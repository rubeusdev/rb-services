<?php
namespace Rubeus\Servicos\Emcrypt;
use Rubeus\Servicos\Entrada\I;

abstract class Emcrypt
{

    public static function getChave(){
        return pack('H*', "bab04b7e123a0cd8b54763051cff02bc55abe029fdebae5e1d417e2ffb2a00a2");
    }

    /* public static function embaralhar($texto){
        $iv_size = \mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);

        $iv = \mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $textoCriptografafia = \mcrypt_encrypt(MCRYPT_RIJNDAEL_128, self::getChave(),
                                     $texto, MCRYPT_MODE_CBC, $iv);
        $textoCriptografafia = $iv . $textoCriptografafia;
        $textoCriptografafia_base64 = base64_encode($textoCriptografafia);
        $textoTratado = str_replace(array('+','/','='),array('-','_','.'),$textoCriptografafia_base64);
        return $textoTratado;
    }

    public static function desembaralhar($textoCriptografafia){
        $textoTratado = str_replace(array('-','_','.'),array('+','/','='),$textoCriptografafia);
        $mod4 = strlen($textoTratado) % 4;
        if ($mod4) {
          $textoTratado .= substr('====', $mod4);
        }
        $textoCriptografafia_dec = base64_decode($textoTratado);
        $iv_size = \mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv_dec = substr($textoCriptografafia_dec, 0, $iv_size);
        $textoCriptografafia_dec = substr($textoCriptografafia_dec, $iv_size);
        $texto = \mcrypt_decrypt(MCRYPT_RIJNDAEL_128, self::getChave(),
                                        $textoCriptografafia_dec, MCRYPT_MODE_CBC, $iv_dec);
        return $texto;
    } */

    public static function embaralhar($message)
    {
        $fileKey = I::server("SODIUM_KEY", '/var/www/vhost/sodium-key3.txt');
        if (!file_exists($fileKey)) {
            $fileKey = __DIR__.'/key.tst';
        }
        $fileNonce = I::server("SODIUM_NONCE", '/var/www/vhost/sodium-key-nonce2.txt');
        if (!file_exists($fileNonce)) {
            $fileNonce = __DIR__.'/nonce.tst';
        }
        $key = file_get_contents($fileKey);
        $nonce = file_get_contents($fileNonce);

        if (in_array(gettype($message), ['NULL', 'double', 'integer', 'boolean'])) {
            $message = (string) $message;
        }
        $cipher = bin2hex(
            sodium_crypto_secretbox($message, $nonce, $key)
        );

        return $cipher;
    }

    public static function desembaralhar($encrypted)
    {
        $fileKey = I::server("SODIUM_KEY", '/var/www/vhost/sodium-key3.txt');
        if (!file_exists($fileKey)) {
            $fileKey = __DIR__.'/key.tst';
        }
        $fileNonce = I::server("SODIUM_NONCE", '/var/www/vhost/sodium-key-nonce2.txt');
        if (!file_exists($fileNonce)) {
            $fileNonce = __DIR__.'/nonce.tst';
        }
        $key = file_get_contents($fileKey);
        $nonce = file_get_contents($fileNonce);

        $plaintext = sodium_crypto_secretbox_open(
            hex2bin($encrypted), $nonce, $key
        );
        return $plaintext;
    }

    public static function codigo($q,$tp='a-z0-9A-Z'){
        preg_match("/[$tp]+/", 'abcdefghijklnopqrstuvwxyzm0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',$out);
        $codigo='';
        while(strlen($codigo) < $q) {
            $codigo .= $out[0][mt_rand( 0, (strlen($out[0]) - 1) )];
        }
        return $codigo;
    }

    public static function gerarCodigoBanco($obj, $atributo, $q, $array=array(),$caracterConfuso=false){
        if($caracterConfuso){
            $caract = 'abcdefghijklnopqrstuvwxyzm0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }else {
            $caract = '23456789ABCDEFGHJKLMNPQRTUVWXYZ';
        }
        $codigo='';
        do{
            while(strlen($codigo) < $q) {
                $codigo .= $caract[mt_rand( 0, (strlen($caract) - 1) )];
            }
            $obj->set($atributo,$codigo);

        }while($obj->carregar('id') || in_array($codigo,$array));


        return $codigo;
    }


    public static function gerarCodigoApenasNumerosBanco($obj, $atributo, $q, $array=array()){
        //Retirados
        //01liILo

        $caract = '123456789';
        $codigo='';
        do{
            while(strlen($codigo) < $q) {
                $codigo .= $caract[mt_rand( 0, (strlen($caract) - 1) )];
            }
            $obj->set($atributo,$codigo);

        }while($obj->carregar('id') || in_array($codigo,$array));


        return $codigo;
    }

}
