<?php
namespace Rubeus\Servicos\Vetor;

abstract class Vetor{
    
    public static function gerarValorPadrao($q,$valor){
        $array = array();
        for($i = 0;$i<intval($q);$i++)
            $array[] = $valor;
        return $array;
    }
    
    public static function mesclar($pr,$seg){
        foreach($seg as $key=>$value)
            $pr[$key] = !isset($pr[$key])?$value:$pr[$key];
        return $pr;
    }
    	
    public static function ordernar($dados, $indice){
        for($i = 1; $i < count($dados); $i++){
            $j = $i;
            while($dados[$j][$indice] < $dados[$j-1][$indice] && $j != 0){
                $aux =  $dados[$j];
                $dados[$j] = $dados[$j - 1];
                $dados[$j - 1] = $aux;
                $j--;
            }
        }
       
        return $dados;
    }
    
    public static function ordernarDes($dados, $indice){
        //var_dump($dados, $indice);
        for($i = 1; $i < count($dados); $i++){
            $j = $i;
            while($j > 0 && $dados[$j][$indice] > $dados[$j-1][$indice]){
                $aux =  $dados[$j];
                $dados[$j] = $dados[$j - 1];
                $dados[$j - 1] = $aux;
                $j--;
            }
        }
        return $dados;
    }
    
    
     public static function arrayObjeto($dados){
        $array = array();
        for($i = 0; $i < count($dados); $i++){
            if(isset($dados[$i]) && !is_null($dados[$i])){
               $array[] = $dados[$i];
           }
        }
       
        return $array;
    }
}
