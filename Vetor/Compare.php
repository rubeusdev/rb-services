<?php

namespace Rubeus\Servicos\Vetor;

class Compare{
    
    function igualdade($array,$array1) {    
        $qtd = count($array);
        $qtd1 = count($array1);
        if($qtd !== $qtd1)return false;
        for($i = 0; $i < $qtd; $i++)
            if($array[$i] !== $array1[$i])return false;
        return true;
    }
    
}