<?php

namespace Rubeus\Servicos\String;

class Caracter{

    public function removerCarctAcen($string){
        $what = array(  'ä','ã','à','á','â','ê','ë','è','é','ẽ','ê','ï','ì','í','ö','õ','ò','ó',
                        'ô','ü','ù','ú','û','Ã','À','Á','É','Ẽ','Ê','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ',
                        '_','(',')',',',';',':','|','!','"','#','$','%','&','/','=','?',
                        '~','^','>','<','ª','º',"'",' ','+','.','@','*','´','`');

        $by   = array(  'a','a','a','a','a','e','e','e','e','e','e','i','i','i','o','o','o','o',
                        'o','u','u','u','u','A','A','A','E','E','E','I','O','U','n','n','c','C','-',
                        '','','','','','','','','','','','','','-','','',
                        '','','','','','','', '','','-','','');
        
        $replaced = str_replace($what, $by, $string);
        $regex = '/(\s|\\\\[rntv]{1})/';
        
        return preg_replace($regex, '', $replaced);
    }

    public function removerCaracterEspecial($string){
        $resposta = preg_replace('#[^a-z0-9]#', '', $string);
        return $resposta;
    }
}
