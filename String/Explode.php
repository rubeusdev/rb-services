<?php

namespace Rubeus\Servicos\String;

class Explode{
    
    public function explode($delimiters,$string) {
        if(is_array($delimiters)) return $this->explodeArray($delimiters, $string);
        if(is_string($delimiters))return explode($delimiters, $string);
        return array($string);
    }
    
    private function explodeArray($delimiters,$string){
        if(count($delimiters) == 0)return array($string);
        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
    }
}