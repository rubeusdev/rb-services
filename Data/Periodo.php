<?php
namespace Rubeus\Servicos\Data;

class Periodo implements InterfacePeriodo{
    private $periodo;

    public function __construct($periodo=0) {
        $this->periodo = $periodo;
    }

    public function novo($periodo) {
        $this->periodo = $periodo;
        return $this;
    }

    public function somar($data){
        $data->add(new \DateInterval($this->periodo));
    }

    public function subtrair($data){
        $data->sub(new \DateInterval($this->periodo));
    }

}

