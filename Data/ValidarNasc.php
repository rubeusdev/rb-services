<?php
namespace Rubeus\Servicos\Data;

class ValidarNasc implements InterfaceValidarData{

     public function validar($data){
        $dataAtual = new \DateTime();
        $diff = $data->diff($dataAtual);
        if($diff->days > 0 && $diff->y < 150 && $data < $dataAtual) return true;
        return false;
    }
}
