<?php
namespace Rubeus\Servicos\Data;

interface InterfacePeriodo{
    
    public function somar($data);
    
    public function subtrair($data);
}

