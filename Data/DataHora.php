<?php
namespace Rubeus\Servicos\Data;

class DataHora
{
    private $formato;
    private $dateTime;
    private $valida = true;

    public function __construct($data=null, $formato = 'Y-m-d H:i:s')
    {
        $this->set($data, $formato);
    }

    public function dataValida()
    {
        return $this->valida;
    }

    private function dividirData($data, $formato)
    {
        if (!preg_match('/^[ T.0-9:\\-\\/]{8,29}/', $data)) {
            $this->valida = false;
            return;
        }
        $sequencia = explode('|', str_replace(array('-',' ',':', '/','.','T'), '|', $formato));
        $valores = explode('|', str_replace(array('-',' ',':','/','.','T'), '|', $data));
        $qtd = count($sequencia);
        if (count($sequencia) != count($valores)) {
            $this->valida = false;
            return;
        }
        $dataFormatada = array();
        for ($i = 0; $i < $qtd; $i++) {
            $this->setar($sequencia[$i], intval($valores[$i]), $dataFormatada);
        }
        if (isset($dataFormatada['milisegundos'])) {
            return $dataFormatada['ano'].'-'.$dataFormatada['mes'].'-'.$dataFormatada['dia'].' '.$dataFormatada['hora'].':'.$dataFormatada['minuto'].':'.$dataFormatada['segundo'].'.'.$dataFormatada['microsegundos'];
        }
        if (isset($dataFormatada['hora'])) {
            return $dataFormatada['ano'].'-'.$dataFormatada['mes'].'-'.$dataFormatada['dia'].' '.$dataFormatada['hora'].':'.$dataFormatada['minuto'].':'.$dataFormatada['segundo'];
        }
        return $dataFormatada['ano'].'-'.$dataFormatada['mes'].'-'.$dataFormatada['dia'];
    }

    public function setar($posicao, $valor, &$data)
    {
        $this->valida = true;
        switch ($posicao) {
            case 'Y': $data['ano'] = $valor; break;
            case 'm':
                $this->valida = ($valor > 12 || $valor < 1)?false:$this->valida;
                $data['mes'] = ($valor < 10) ? '0'.$valor : $valor;
                break;
            case 'd':
                $this->valida = ($valor > 31 || $valor < 1)?false:$this->valida;
                $data['dia'] = ($valor < 10) ? '0'.$valor : $valor;
                break;
            case 'H':
                $this->valida = ($valor > 23 || $valor < 0)?false:$this->valida;
                $data['hora'] = ($valor < 10) ? '0'.$valor : $valor;
                break;
            case 'i':
                $this->valida = ($valor > 59 || $valor < 0)?false:$this->valida;
                $data['minuto'] = ($valor < 10) ? '0'.$valor : $valor;
                break;
            case 's':
                $this->valida = ($valor > 59 || $valor < 0)?false:$this->valida;
                $data['segundo'] = ($valor < 10) ? '0'.$valor : $valor;
                break;
            case 'u':
                $this->valida = true;
                $data['microsegundos'] = $valor;
                break;
        }
    }

    public function getFormato()
    {
        return $this->formato;
    }

    public function &setFormato($formato='Y-m-d H:i:s')
    {
        $this->formato = $formato;
        return $this;
    }

    public function getDateTime()
    {
        return $this->dateTime;
    }

    public function get($formato = false)
    {
        /*$backtrace = debug_backtrace();
        var_dump($backtrace[0]['class']);
        var_dump($backtrace[1]['class']);
        var_dump($backtrace[2]['class']);
        var_dump($backtrace[3]['class']);
        var_dump($entidade);*/
        if ($formato) {
            $this->formato = $formato;
        }
        return $this->dateTime->format($this->formato);
    }

    public function &set($data=null, $formato = false)
    {
        if ($formato) {
            $this->formato = $formato;
        }
        if (is_null($data)) {
            if (strpos($this->formato, 'u')) {
                $this->dateTime = DateTime::createFromFormat('U.u', microtime(true));
            } else {
                $this->dateTime = new \DateTime();
            }
        } else {
            $dataFormatada = $this->dividirData($data, $this->formato);
            if (!$this->dataValida()) {
                return $this;
            }
            try {
                $this->dateTime = new \DateTime($dataFormatada);
            } catch (\Exception $e) {
                return  $this;
            }
        }
        return $this;
    }

    public function validarData($tipo)
    {
        if (!$this->hora) {
            return $tipo->validar($this->dateTime);
        }
        return $tipo->validar($this->dateTime);
    }

    public function add($periodo)
    {
        $periodo->somar($this->dateTime);
        return $this;
    }

    public function sub($periodo)
    {
        $periodo->subtrair($this->dateTime);
        return $this;
    }

    public function diff($data)
    {
        if ($this->dateTime instanceof \DateTime) {
            return $this->dateTime->diff($data->getDateTime());
        }
        return false;
    }
    /*
     * -1 segunda data menor q a primeira
     *  0 data iguais
     *  1 segunda data maior q a primeira
     */
    public function compare($data)
    {
        if ($this->dateTime instanceof \DateTime) {
            if ($this->dateTime > $data->getDateTime()) {
                return -1;
            }
            if ($this->dateTime == $data->getDateTime()) {
                return 0;
            }
            if ($this->dateTime < $data->getDateTime()) {
                return 1;
            }
        }
        return false;
    }


    public function ultimoDiaMes()
    {
        if ($this->dateTime instanceof \DateTime) {
            return $this->dateTime->modify('last day of +0 months')->format('d');
        }
        return false;
    }
}
