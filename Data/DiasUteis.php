<?php
namespace Rubeus\Servicos\Data;
use Rubeus\ContenerDependencia\Conteiner;

class DiasUteis implements InterfacePeriodo{
    private $dias;

    public function __construct($dias=0) {
        $this->dias = $dias;
    }

    public function novo($dias){
        $this->dias = $dias;
        return $this;
    }

    public function somar($data){
        $time = strtotime($data->format('Y-m-d'));
        $dias = 0;
        for($i=0;$i<$this->dias;){
            $i += $this->diasUteis("+".$dias." day", $time);
            $dias++;
        }
        $data->add(new \DateInterval('P'.$dias.'D'));
    }

    public function subtrair($data){
        $time = strtotime($data->get('Y-m-d'));
        $dias = 0;
        for($i=0;$i<$this->dias;){
            $i += $this->diasUteis("-".$dias." day", $time);
            $dias++;
        }
        $data->sub(new \DateInterval('P'.$dias.'D'));
    }

    private function diasUteis($x,$time){
        $diaSemana = date("w", strtotime( $x, $time));
        if($diaSemana != 6 && $diaSemana != 0){
            if(!Conteiner::get('DataNaoUteis') || !in_array(date("Y-m-d", strtotime($x, $time)), Conteiner::get('DataNaoUteis')->listaFeriados())){
                return 1;
            }
        }
        return 0;
    }
}
