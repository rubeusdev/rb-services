<?php
namespace Rubeus\Servicos\Data;

class Hora{
    
    public function parseFloat($hora,$tp='s'){
        $array = explode(':',$hora);
        if(count($array) == 3){
            $num = $array[0]*60*60+$array[1]*60+$array[2];
        }else{
            $num = $array[0]*60*60+$array[1]*60;
        }
        switch ($tp){
            case 's':return $num;
            case 'm':return $num/60;
            case 'h':return $num/(60*60);
        }    
        
    }
    
    public function parseString($numHora,$tp='s'){
       switch ($tp){
            case 's':
                $hora =  \intval($numHora/(60*60));
                $minuto =  \intval(($numHora - ($hora * (60*60)))/60);
                $segundo =  (($numHora - ($hora * (60*60))) - ($minuto *60));
                
                $hora = $hora > 10?$hora:'0'.$hora;                
                $minuto = $minuto > 10?$minuto:'0'.$minuto;
                $segundo = $segundo > 10?$segundo:'0'.$segundo;
                
                return $hora.':'.$minuto.':'.$segundo;                
            case 'm':
                $hora =  \intval($numHora/(60));
                $minuto =  $numHora - ($hora * (60));
                $hora = $hora > 10?$hora:'0'.$hora;                
                $minuto = $minuto > 10?$minuto:'0'.$minuto;
                return $hora.':'.$minuto;
        }
        return $hora;     
    }
    
        
    public function parsePeriodo($numHora,$tp='s'){
       switch ($tp){
            case 's':
                $hora =  \intval($numHora/(60*60));
                $minuto =  \intval(($numHora - ($hora * (60*60)))/60);
                $segundo =  (($numHora - ($hora * (60*60))) - ($minuto *60));
                
                $hora = $hora > 10?$hora:'0'.$hora;                
                $minuto = $minuto > 10?$minuto:'0'.$minuto;
                $segundo = $segundo > 10?$segundo:'0'.$segundo;
                
                return 'PT'.$hora.'H'.$minuto.'M'.$segundo.'S';                
            case 'm':
                $hora =  \intval($numHora/(60));
                $minuto =  $numHora - ($hora * (60));
                $hora = $hora > 10?$hora:'0'.$hora;                
                $minuto = $minuto > 10?$minuto:'0'.$minuto;
                return 'PT'.$hora.'H'.$minuto.'M';
        }
        return $hora;     
    }

}
