<?php
namespace Rubeus\Servicos\XML;

abstract class XML{
    
    public static function ler($caminho){
        return file_exists($caminho) ? simplexml_load_file($caminho) : false;
    }
    
}