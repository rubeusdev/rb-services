<?php
namespace Rubeus\Servicos\Pdf;

class DomPdf{
    
    public function gerarPDF($html){
        $dompdf = new \DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("/var/www/paginas/inedu/primeiroPDF.pdf");
    }
    
}