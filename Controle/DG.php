<?php
namespace Rubeus\Servicos\Controle;

abstract class DG{
    static $dados;
    
    public static function get($string, $valorPadrao=false){
        if(is_string($string))
            $var = explode(',',$string);
        else $var = $string;
        $aux = self::$dados;
        foreach($var as $v){
            if(isset($aux[$v])){
                $aux = $aux[$v];
            }else return $valorPadrao;
        }
        return $aux;
    }
    
    public static function set($valor, $string=false){
        if($string === false){
            self::$dados = $valor;
            return ;
        }
        $var = explode(',',$string);
        if(is_array($var)){
            if(count($var)==1)self::$dados[$var[0]] = $valor;            
            else if(count($var)==2)self::$dados[$var[0]][$var[1]] = $valor;            
            else if(count($var)==3)self::$dados[$var[0]][$var[1]][$var[2]] = $valor;
            else if(count($var)==4)self::$dados[$var[0]][$var[1]][$var[2]][$var[3]] = $valor;
            else if(count($var)==5)self::$dados[$var[0]][$var[1]][$var[2]][$var[3]][$var[4]] = $valor;
        }else self::$dados[$var] = $valor;        
    }

    public static function limpar($string = false){
        if($string === false){
            self::$dados = null;
            return ;
        }
       
        $var = explode(',',$string);

        if(is_array($var)){
            if(count($var)==1)unset(self::$dados[$var[0]]);            
            else if(count($var)==2)unset(self::$dados[$var[0]][$var[1]]);            
            else if(count($var)==3)unset(self::$dados[$var[0]][$var[1]][$var[2]]);
            else if(count($var)==4)unset(self::$dados[$var[0]][$var[1]][$var[2]][$var[3]]);
            else if(count($var)==5)unset(self::$dados[$var[0]][$var[1]][$var[2]][$var[3]][$var[4]]);
        }else unset(self::$dados[$var]);
        
    }
    
    
}

