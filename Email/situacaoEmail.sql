/*
-- Query: select * from situacaoenvioemail
-- Date: 2015-03-26 14:06
*/
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (1,'Email enviado','email enviado com sucesso.',now(),1);
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (2,'SMTP - NÃ£o autenticou','SMTP Erro: NÃ£o foi possÃ­vel autenticar.',now(),1);
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (3,'SMTP - NÃ£o conectou','SMTP Erro: NÃ£o foi possÃ­vel conectar ao host SMTP.',now(),1);
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (4,'SMTP - NÃ£o autenticou','Erro de SMTP : Os dados nÃ£o sÃ£o aceites.',now(),1);
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (5,'Sem corpo',' O corpo da mensagem vazio',now(),1);
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (6,'codificaÃ§Ã£o Desconhecido','codificaÃ§Ã£o Desconhecido',now(),1);
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (7,'NÃ£o foi possÃ­vel executar','NÃ£o foi possÃ­vel executar',now(),1);
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (8,'NÃ£o foi possÃ­vel acessar o arquivo','NÃ£o foi possÃ­vel acessar o arquivo',now(),1);
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (9,'NÃ£o foi possÃ­vel abrir o arquivo','Erro de arquivo : NÃ£o foi possÃ­vel abrir o arquivo:',now(),1);
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (10,'endereÃ§o De falhou:','O seguinte endereÃ§o De falhou:','2015-02-12 17:22:13',1);
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (11,'Erro funÃ§Ã£o mail','NÃ£o foi possÃ­vel instanciar a funÃ§Ã£o mail.',now(),1);
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (12,'endereÃ§o invÃ¡lido','endereÃ§o invÃ¡lido','2015-02-12 17:22:13',1);
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (13,'mailer nÃ£o Ã© suportado','mailer nÃ£o Ã© suportado',now(),1);
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (14,'Sem destinatÃ¡rio.','VocÃª deve fornecer pelo menos um endereÃ§o de e-mail do destinatÃ¡rio.',now(),1);
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (15,'SMTP - destinatÃ¡rios falhou','Erro de SMTP : Os seguintes destinatÃ¡rios falhou',now(),1);
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (16,'O erro de asinatura','A assinatura de erro:',now(),1);
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (17,'SMTP Connect() falhou','SMTP Connect() falhou',now(),1);
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (18,'erro no servidor SMTP','erro no servidor SMTP',now(),1);
INSERT INTO `situacaoenvioemail` (`id`,`titulo`,`descricao`,`momento`,`ativo`) VALUES (19,'erro variÃ¡vel','NÃ£o Ã© possÃ­vel definir ou redefinir variÃ¡vel',now(),1);
