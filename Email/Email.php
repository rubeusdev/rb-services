<?php
namespace Rubeus\Servicos\Email;
require __DIR__.'/PHPMailer.class.php';
//$email->ErrorInfo
class Email{
    private $enviado;
    private $mail;
    private $arquivo;

    public function __construct($id=false) {
        $this->iniciar($id);
    }

    public function inserirArquivo(){
        if(!$this->arquivo){
            return;
        }
        if(is_array($this->arquivo)){
            foreach($this->arquivo as $a){
                $this->mail->AddAttachment($a);
            }
        }else{
            $this->mail->AddAttachment($this->arquivo);
        }
    }

    public function setAssunto($assunto){
        $this->mail->Subject = $assunto;
    }

    public function setTexto($texto){
        $this->mail->Body = $texto;
    }

    public function setDestino($destino){
        $this->mail->AddAddress($destino);
    }

    public function setCopiaOculta($destino){
        $this->mail->AddBcc($destino);
    }

    public function setArquivo($arquivo){
        $this->arquivo = $arquivo;
    }

    public function setEmailResposta($emailResposta){
        $this->mail->AddReplyTo($emailResposta);
    }

    public function setRemetente($email, $nome){
        $this->mail->SetFrom($email, $nome);
    }

    private function iniciar($obj){
        $this->mail = new \PHPMailer();
        $this->mail->IsSMTP();
        $this->mail->SMTPAuth =  true;  // Define que a mensagem será SMTP
        $this->mail->Host = $obj->host; // Endereço do servidor SMTP
        $this->mail->Port = $obj->porta; // Porta SMTP do GMAIL  // Caso o servidor SMTP precise de autenticação
        $this->mail->CharSet = 'UTF-8';
        $this->mail->SMTPSecure = $obj->SMTPSecure;
        // $this->mail->SMTPDebug = 1;
        $this->mail->Username = $obj->userName; // SMTP username
        $this->mail->Password = $obj->password;
        $this->mail->From = $obj->from; // Seu e-mail
        $this->mail->FromName = $obj->fromName;
        //$this->mail->AddBcc('jgt.josegeraldo@gmail.com');
        $this->mail->IsHTML(true);
        $this->arquivo = false;
    }

    public function enviar(){
        $this->enviado=0;
        $this->inserirArquivo();
        $retorno = $this->mail->Send();
        $this->mail->ClearAllRecipients();
        $this->mail->ClearAttachments();
        $this->enviado = intval($retorno);
        if($this->enviado == 1)return true;
        else return false;
    }

    public function getErroCodigo(){
        //var_dump($this->mail->ErrorInfo);
        return  substr($this->mail->ErrorInfo, 0, strpos($this->mail->ErrorInfo,'-'));
    }
    
    public function getErroCodigoCompleto(){
        //var_dump($this->mail->ErrorInfo);
        // return  substr($this->mail->ErrorInfo, 0, strpos($this->mail->ErrorInfo,'-'));
        return  $this->mail->ErrorInfo;
    }
}
