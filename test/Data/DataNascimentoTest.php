<?php

use PHPUnit\Framework\TestCase;
use Rubeus\Servicos\Data\ValidarNasc;

class ValidarNascTest extends TestCase{
    private $validarData;

    public function __construct(){
        $this->validarData = new ValidarNasc();
    }

    public function testDataInvalidaFutura(){
        $this->assertFalse($this->validarData->validar(new \DateTime('2117-01-01')),'PROBLEMA DE VALIDAÇÃO DE DATAS FUTURAS');
    }

    public function testDataInvalidaPassado(){
        $this->assertFalse($this->validarData->validar(new \DateTime('1856-01-01')),'PROBLEMA DE VALIDAÇÃO DE DATAS PASSADAS');
    }

    public function testDataValida(){
        $this->assertTrue($this->validarData->validar(new \DateTime('2013-01-01')),'PROBLEMA DE VALIDAÇÃO DE DATAS VALIDAS');
    }

}
