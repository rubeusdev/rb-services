<?php

use PHPUnit\Framework\TestCase;
use Rubeus\Servicos\Data\DataFutura;

class DataFuturaTest extends TestCase{
    private $validarDataFutura;

    public function __construct(){
        $this->validarDataFutura = new DataFutura();
    }

    public function testDataPassada(){
        $this->assertFalse($this->validarDataFutura->validar(new \DateTime('2007-01-01')),'PROBLEMA DE VALIDAÇÃO DE DATAS PASSADAS');
    }

    public function testDataAtual(){
        $this->assertFalse($this->validarDataFutura->validar(new \DateTime()),'PROBLEMA DE VALIDAÇÃO DE DATAS ATUAIS');
    }

    public function testDataFutura(){
        $this->assertTrue($this->validarDataFutura->validar(new \DateTime('2113-01-01')),'PROBLEMA DE VALIDAÇÃO DE DATAS FUTURAS');
    }

}
